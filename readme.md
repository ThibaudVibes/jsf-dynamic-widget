Tableau de bord dynamique
===========

Ce projet est un POC de tableau de bord dynamique. L'idée est de valider la possibilité de manipuler 
l'arbre JSF programmatiquement pour choisir ce qu'on affiche à runtime.

### Prérequis

Ce POC a été développé et testé avec les éléments suivants :

* Java 1.8 minimum
* JSF 2.2
* Payara 4.1.1.163


### Build

Pour construire le projet executez la commande suivante :
```
mvn clean package
``` 


### Ressources

Safely manipulating the component tree in JSF 2
http://blog.kennardconsulting.com/2009/11/safely-manipulating-component-tree-with.html
Revisited 
http://blog.kennardconsulting.com/2010/10/safely-manipulating-component-tree-with.html

Add a resource component to the head target of the view :
http://stackoverflow.com/a/4919852

JSF Component listener with @ListenerFor
http://memorynotfound.com/jsf-component-listener/
