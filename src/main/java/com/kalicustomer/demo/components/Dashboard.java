package com.kalicustomer.demo.components;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ListenerFor;
import javax.faces.event.ListenersFor;
import javax.faces.event.PostAddToViewEvent;
import javax.faces.event.PostConstructViewMapEvent;

@FacesComponent(value = "com.kalicustomer.demo.comp", createTag = true, tagName = "dashboard", namespace = "http://demo.kalicustomer.com/components")
@ListenersFor({ @ListenerFor(systemEventClass = PostAddToViewEvent.class),
		@ListenerFor(systemEventClass = PostConstructViewMapEvent.class) })
public class Dashboard extends UIOutput {

	private static List<String> libraries = Arrays.asList("magasin", "region", "siège");

	protected enum PropertyKeys {
		library,;
		String toString;

		PropertyKeys(String toString) {
			this.toString = toString;
		}

		PropertyKeys() {
		}

		public String toString() {
			return ((toString != null) ? toString : super.toString());
		}
	}

	/**
	 * <p>
	 * Return the value of the <code>library</code> property.
	 * </p>
	 */
	public java.lang.String getLibrary() {
		return getStateHelper().eval(PropertyKeys.library) != null
				? (java.lang.String) getStateHelper().eval(PropertyKeys.library) : libraries.get(0);

	}

	/**
	 * <p>
	 * Set the value of the <code>library</code> property.
	 * </p>
	 */
	public void setLibrary(java.lang.String library) {
		getStateHelper().put(PropertyKeys.library, library);
	}

	public void processEvent(ComponentSystemEvent event) throws AbortProcessingException {
		if (event instanceof PostAddToViewEvent) {
			UIOutput resource = new UIOutput();
			resource.getAttributes().put("name", "css/grid.css");
			resource.getAttributes().put("library", "default");
			resource.setRendererType("javax.faces.resource.Stylesheet");
			FacesContext.getCurrentInstance().getViewRoot().addComponentResource(FacesContext.getCurrentInstance(),
					resource);
		}

		if ("magasin".equals(getLibrary())) {
			UIComponent row0 = addRow();
			addWidget(row0, 6, "images/nps-big.png");
			addWidget(row0, 6, "images/nps-evolution.png");

			UIComponent row1 = addRow();
			addBlank(row1, 1);
			addWidget(row1, 4, "images/top4.png");
			addBlank(row1, 2);
			addWidget(row1, 4, "images/top-mention.png");
		}
		
		if("siege".equals(getLibrary())){
			UIComponent row0 = addRow();
			addWidget(row0, 12, "images/metriques4.png");
			
			UIComponent row1 = addRow();
			addWidget(row1, 6, "images/nps-big.png");
			addWidget(row1, 6, "images/comparaison.png");

			UIComponent row2 = addRow();
			addBlank(row1, 1);
			addWidget(row2, 4, "images/top-item-colors.png");
			addBlank(row2, 2);
			addWidget(row2, 4, "images/top-mention.png");			
		}
		super.processEvent(event);
	}

	@Override
	public void encodeEnd(FacesContext context) throws IOException {
		ResponseWriter responseWriter = context.getResponseWriter();
		responseWriter.startElement("div", null);
		{
			responseWriter.startElement("div", null);
			{
				responseWriter.writeAttribute("id", getClientId(context), null);
				responseWriter.writeAttribute("class", "title", null);
				responseWriter.write((String) getValue());
			}
			responseWriter.endElement("div");

			renderChildren(context, this);
		}
		responseWriter.endElement("div");
	}

	/**
	 * Render all children for given component.
	 *
	 * @param facesContext
	 * @param component
	 * @throws IOException
	 */
	public void renderChildren(FacesContext facesContext, UIComponent component) throws IOException {
		if (component.getChildCount() > 0) {
			for (UIComponent child : component.getChildren()) {
				child.encodeAll(facesContext);
			}
		}
	}

	/**
	 * Ajoute une ligne au dashboard et la renvoie
	 * 
	 * @return
	 */
	private UIComponent addRow() {
		FacesContext ctx = FacesContext.getCurrentInstance();
		HtmlPanelGroup row = (HtmlPanelGroup) ctx.getApplication().createComponent(HtmlPanelGroup.COMPONENT_TYPE);
		row.setStyleClass("row");

		getChildren().add(row);

		return row;
	}

	/**
	 * Ajoute un "widget" - Dans ce POC les widget sont des images
	 * 
	 * @param row
	 * @param nbCols
	 * @param name
	 */
	private void addWidget(UIComponent row, int nbCols, String name) {
		FacesContext ctx = FacesContext.getCurrentInstance();
		HtmlGraphicImage widget = (HtmlGraphicImage) ctx.getApplication()
				.createComponent(HtmlGraphicImage.COMPONENT_TYPE);
		widget.setStyleClass("col-" + nbCols);
		final String library = getLibrary() != null ? getLibrary() : libraries.get(0);
		widget.getAttributes().put("name", name);
		widget.getAttributes().put("library", library);

		row.getChildren().add(widget);
	}

	/**
	 * Ajoute un "esapce vide" sur la ligne
	 * 
	 * @param row
	 * @param nbCols
	 * 
	 */
	private void addBlank(UIComponent row, int nbCols) {
		FacesContext ctx = FacesContext.getCurrentInstance();
		HtmlPanelGroup blank = (HtmlPanelGroup) ctx.getApplication().createComponent(HtmlPanelGroup.COMPONENT_TYPE);
		blank.setStyleClass("blank col-" + nbCols);

		row.getChildren().add(blank);
	}
}
